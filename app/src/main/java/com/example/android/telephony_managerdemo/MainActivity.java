package com.example.android.telephony_managerdemo;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.widget.Switch;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView mPhoneDetailText;
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPhoneDetailText=findViewById(R.id.DetailText);

        TelephonyManager telephonyManager= (TelephonyManager) getSystemService
                (Context.TELEPHONY_SERVICE);

        String IMEI_NO=telephonyManager.getImei(1);
        String subscriberId=telephonyManager.getSubscriberId();
        String SimSerial_No=telephonyManager.getSimSerialNumber();
        String networkCountry_Iso=telephonyManager.getNetworkCountryIso();
        String simCountryIso=telephonyManager.getSimCountryIso();
        String softwareVersion=telephonyManager.getDeviceSoftwareVersion();
        String voiceMailNo=telephonyManager.getVoiceMailNumber();

        String smartPhoneType="";

        int phoneType=telephonyManager.getPhoneType();

        switch(phoneType){
            case(TelephonyManager.PHONE_TYPE_CDMA):
                smartPhoneType="CDMA";
                break;
            case(TelephonyManager.NETWORK_TYPE_GSM):
                smartPhoneType="GSM";
                break;
            case(TelephonyManager.PHONE_TYPE_NONE):
                smartPhoneType="NONE";
                break;
            case (TelephonyManager.PHONE_TYPE_SIP):
                smartPhoneType="SIP";
                break;
            default:
                smartPhoneType="UnKnown";

        }

        boolean isRoaming=telephonyManager.isNetworkRoaming();

        String info=" " +
                "IMEI NO: "+IMEI_NO;
        info+="\n Subscriber ID: "+subscriberId;
        info+="\n Sim Card Serial No: "+SimSerial_No;
        info+="\n Newtwork Country: "+networkCountry_Iso;
        info+="\n Sim Card Country: "+simCountryIso;
        info+="\n Software Version of Phone: "+softwareVersion;
        info+="\n Voice Mail No: "+voiceMailNo;
        info+="\n Smart Phone Type: "+smartPhoneType;
        info+="\n Phone in Roaming: "+isRoaming;

        mPhoneDetailText.setText(info);

    }
}
